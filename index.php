<?php
/* render the web interface:
 * steps:
 * ask the database for all projectors and if they're running. build the
 * navigation section using that
 * 
 * use the $_GET variable to know what projector to display on the page. if
 * it's empty, use the first option. build the power button (disabled if the
 * user doesn't have access) and write generic data
 * 
 * if admin, load 5 log entries and calculate statistics with them
 * 
 * note that this script doesn't write to the database in any way. it passes
 * any work to update.php - the "backend" - which does that
 */

// ask the browser to refresh every 5 minutes seconds
header("Refresh: 300");
// stackoverflow.com/q/13640109/prevent-browser-from-caching-php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
 
require_once('auth.php');
require_once('database.php');

// load all projectors and their active session (if running; else NULL)
$sql = <<<SQL
   SELECT `projectors`.`type`,
          `projectors`.`room`,
          `projectors`.`comment`,
          `running`.`session`,
          `running`.`start_by` AS `by`
     FROM `projectors`
LEFT JOIN ( SELECT `room`, `session`, `start_by`
              FROM `logs`
             WHERE `logs`.`end` = TIMESTAMP(0) )
       AS `running`
       ON `running`.`room` = `projectors`.`room`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$list = $statement->fetchAll(PDO::FETCH_GROUP);

$user = $_SERVER['REMOTE_USER'];
$auth = auth_level($user);

$now = (new DateTime())->format('l, M j \a\t g:i a');
// rename $content -> $c
$c = "<p>Last updated: $now</p><nav id='list'>";

// stackoverflow.com/q/1921421/first-element-of-array
reset($_GET);
$view = key($_GET);
if ($view === NULL) {
    // load the first projector instead - database must have 1 projector
    $view = reset($list)[0]['room'];
}
// true when $view is validated in the loop below
$view_ok = false;
// true if the $ok'd projector is running
$view_on = false;
// this is the only field (for now) which needs to be accessed in <main>
$view_comment = "";

// navigation
foreach ($list as $type => $rooms) {
    $c .= "<h2>{$type}s</h2><ul>";
    foreach ($rooms as $group) {
        // sets $room, $session, and $by
        // stackoverflow.com/q/28232945/destructuring-associative-arrays
        extract($group);
        $on = $session !== NULL;
        $ok = $view === $room;
        // as we iterate, validate the view and save it's on state
        if ($view_ok === false) {
            $view_ok = $ok;
            $view_on = $on;
            $view_comment = $comment;
        }
        $disabled = ($user !== $by and $auth !== 'admin');
        $div = ($ok ? 'selected' : '');
        $a = 'switch' . ($on ? ' on' : '') . ($disabled ? ' disabled' : '');
        $update = $room . '=' . ($on ? '0' : '1');
        $c .= "<div class='$div'><a href='/?$room'><li>$room</li></a>"
           .  "<a class='$a' href='/update.php?$update'></a></div>";
    }
    $c .= "</ul>";
}
$c .= "</nav><main id='projector'><div>";

// projector page
// functions need globals. using goto. stackoverflow.com/q/7468836/break-if
if ($view_ok === false) {
    // should only happen if editing the URL
    $c .= "<p>Projector does not exist. Check URL</p></div>";
    goto done;
}
// $view has been validated and can now be used
$c .= "<h1>ELW $view</h1>";
$sql = <<<SQL
   SELECT `session`,
          DATE_FORMAT(`start`, '%b %e') AS `date`,
          DATE_FORMAT(`start`, '%l:%i %p') AS `start`,
          CONCAT(
              TIMESTAMPDIFF(
                  MINUTE, `start`, IF (`end` = TIMESTAMP(0), NOW(), `end`)
              ), ' min') AS `duration`,
          `start_by` AS `by`,
          `end_reasons`.`text` AS `via`
     FROM `logs`
LEFT JOIN `end_reasons`
       ON `end_reason` = `id`
    WHERE `room` = ?
 ORDER BY `date`, `start` DESC
    LIMIT 10
SQL;
$statement = $db->prepare($sql);
$statement->execute(array($view));
$logs = $statement->fetchAll();

if ($view_on) {
    // there is at least one log if $view_on is set
    $started = $logs[0]['date'] . ', ' . $logs[0]['start'];
    // in PHP you can do '10' (string) - '5 word' (string) --> 5 (number)
    $left = 3 * 60 - $logs[0]['duration'];
    $c .= "<p>Running since $started - $left minutes left</p>"
       .  "<a class='btn' href='/update.php?$view=1'>New session</a>"
       .  "<p>You can't turn off another person's projector but you can "
       .  "request a new session to reset the timeout</p>";
} else {
    $c .= "<p>Idle</p>";
}
// $view_comment will accurate reflect the projector since $view was validated
$c .= "<hr>"
   .  "<p>$view_comment</p>"
   .  "</div>";
// logs
if ($auth !== 'admin') {
    goto done;
}
$c .= "<h2>Logs</h2>";
if (empty($logs)) {
    $c .= "<div><p>No logs</p></div>";
    goto done;
}
$heads = "";
foreach (['#', 'Date', 'Started', 'Duration', 'By', 'Ended via'] as $th) {
    $heads .= "<th>$th</th>";
}
$c .= "<table id='logs'><thead><tr>$heads</tr></thead><tbody>";
foreach ($logs as $log) {
    $row = "";
    foreach ($log as $data) {
        $row .= "<td>$data</td>";
    }
    $c .= "<tr>$row</tr>";
}
$c .= "</tbody></table>";

// exit
done:
$c .= "</main>";

// configure and load the page template
$display_name = "ENGR";
$page_title = "Projector Control";
$content = &$c;

require_once($_SERVER['DOCUMENT_ROOT'] . '/template/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/template/blank-edge-engn.php');
?>
