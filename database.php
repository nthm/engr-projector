<?php
/* initializes a connection to the database
 * 
 * if called with the argument 'init' with tables `projectors` and `logs` are
 * created, overwriting the data, and default entries are added 
 */

$db_host = '[redacted]';
$db_db   = 'projector';
$db_user = 'projector';
$db_pass = '[redacted]';
$db_char = 'utf8';

global $db;
try {
    $db = new PDO("mysql:host=$db_host;dbname=$db_db;charset=$db_char",
        $db_user, $db_pass,
        array(
            PDO::ATTR_PERSISTENT => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        )
    );
} catch (PDOException $error) {
    echo "couldn't connect to projector database\n";
    echo $error->getMessage();
    exit();
}

if (!isset($argv[1]) || ($argv[1] != 'init')) return;

echo "overwriting database\n";

/* the projector may be turned on by a remote or the power button, and this
 * is considered wrong, so systemd-timer will notice it through snmpwalk, and
 * turn it off. therefore any unauthorized sessions are limited to 5 minutes
 * 
 * could easily add `lamp_hours`, or settings like `keystone`, etc in here
 * 
 * added `type` for labs and meetings. could use another table for it - chose
 * VARCHAR over ENUM because we're not sure how many groups there will be
 */
$projectors_table = <<<SQL
CREATE TABLE `projectors` (
    `room`    VARCHAR(5)  NOT NULL,
    `type`    VARCHAR(10) NOT NULL,
    `url`     VARCHAR(50) NOT NULL,
    `user`    VARCHAR(50) NOT NULL,
    `pass`    VARCHAR(50) NOT NULL,
    `comment` TEXT,
    PRIMARY KEY (`room`) 
);
SQL;
$db->query($projectors_table);

// reasons for ending a session in the logs table
$reason_table = <<<SQL
CREATE TABLE `end_reasons` (
    `id`   INT         NOT NULL AUTO_INCREMENT,
    `text` VARCHAR(50) NOT NULL,
    PRIMARY KEY (`id`)
);
INSERT INTO `end_reasons` (`text`)
VALUES
    ('off (web)'          ),
    ('off (remote/button)'),
    ('unauthorized'       ),
    ('session transferred'),
    ('timed out'          ),
    ('connection lost'    );
SQL;
$db->query($reason_table);

/* tried the `ended` column as NOT NULL (even though it can be empty) as only
 * one row can have that value empty at a time, but MySQL's UNIQUE INDEX is OK
 * with multiple NULLs per column
 * 
 * see these links and versions/notes/not_null_timestamp.txt for details:
 * stackoverflow.com/q/3712222/mysql-ignores-null-on-unique-constraints
 * stackoverflow.com/q/15800250/unique-combination-of-two-columns
 * 
 * switched from DATETIME to TIMESTAMP (which is only supported until 2038) to
 * use DEFAULT times and not need to write NOW() each INSERT
 * 
 * DEFAULT CURRENT_TIMESTAMP works for both DATETIME and TIMESTAMP as of MySQL
 * 5.6.5, and DATETIME should replace TIMESTAMP as soon as MySQL is updated
 * see: dba.stackexchange.com/q/132951/timestamp-as-default-value-in-mysql
 * 
 * the `start_by` column should be joined with the role system in LDAP
 */
$logs_table = <<<SQL
CREATE TABLE `logs` (
    `session`    INT         NOT NULL AUTO_INCREMENT,
    `room`       VARCHAR(5)  NOT NULL,
    `start`      TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `start_by`   VARCHAR(50) NOT NULL,
    `end`        TIMESTAMP   NOT NULL DEFAULT 0,
    `end_reason` INT,
    PRIMARY KEY (`session`),
    FOREIGN KEY (`room`) REFERENCES `projectors`(`room`),
    FOREIGN KEY (`end_reason`) REFERENCES `end_reasons`(`id`)
);
ALTER TABLE `logs`
     ADD UNIQUE `uq_logs`(`room`, `ended`);
SQL;
$db->query($logs_table);

// build some test data
$sql = <<<SQL
INSERT INTO `projectors` (`room`, `type`, `url`, `user`, `pass`, `comment`)
VALUES
    ('b203', 'lab'    , 'b203-projector', 'EPSONWEB', '[redacted]', 'Support, see Lynn ELW'),
    ('b220', 'lab'    , 'b220-projector', 'EPSONWEB', '[redacted]', 'Support, see Lynn ELW'),
    ('b215', 'lab'    , 'b215-projector', 'EPSONWEB', '[redacted]', 'Support, see Lynn ELW'),
    ('w230', 'meeting', 'w230-projector', 'EPSONWEB', '[redacted]', 'Not yet setup');
SQL;
$db->query($sql);

$sql = <<<SQL
INSERT INTO `logs` (`room`, `start`, `start_by`, `end`, `end_reason`)
VALUES
    ('b203', '2017-08-16 13:10:00', 'test', '2017-08-16 13:20:00', 1),
    ('b220', '2017-08-17 14:50:20', 'test', '2017-08-17 15:00:00', 1),
    ('b215', '2017-08-02 12:00:00', 'test', '2017-08-02 12:45:00', 2);
SQL;
$db->query($sql);

echo "OK inserted 3 rows\n";
?>
