# projectors.engr.uvic.ca

allows staff and students to remotely control projectors in the engineering
faculty. completed while working with UVic Engineering IT

the screenshot below is _not_ the live version, but I no longer have access to
the administration page and it was the last picture taken

![](./versions/v2/v2.jpg)

a campus netlink account requires `services:projector:{view,power,admin}` to
connect to the application.

- `view` allows access to the web interface to see which projectors are in use

- `power` lets you turn on projectors and adopt someone's session. to properly
log the projector's use, you can't turn off someone else's projector.

- `admin` displays the log for each projector and allows you to end any
session

#### index.php

builds the web interface. UVic does not use a framework so involves `echo`ing
HTML as the script runs

there is no frontend or Javascript in this project

#### update.php

controls the projectors. each time the script is called, it checks the status
of each projector in the database - cases to consider:

- is the projector running when it should be off?
- has a session timed out?
- does the user have permission to control the projector?

a systemd timer+service runs update.php every 5 minutes, which means it is the
worst case length of any unauthorized use

#### sessions

when a projector is turned on in the application, a session is started. there
are 6 ways a session can end:

##### off (web)
expected behaviour - turned off via web interface

##### off (remote/button)
a projector marked as on was discovered off - that's ok! someone pressed the
power button or used the remote. *note*: this is different than _connection
lost_ because the projector can be pinged over the network

##### unauthorized
a projector marked as off was discovered on - not ok. this is will create a
log entry of 0 minutes because there will be no session to mark as ended

##### session transferred
used to reset the session timeout (by making a new session) or for cases where
someone wants to use a projector without having it shutdown and turn back on

##### timed out
a session was longer than 3 hours

##### connection lost
the projector cannot be pinged over the network. someone may have unplugged
a network cable
 
see `update.php` and `versions/notes/decision_tree.jpg` for details
