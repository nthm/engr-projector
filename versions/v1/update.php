<?php
require_once('database.php');
global $db;

// https://stackoverflow.com/a/7655379
function info($msg) {
    echo $msg;
    //error_log($msg, 3, 'update.log');
}

// all the projectors
$sql = <<<SQL
SELECT `room`, `url`, `user`, `pass`
  FROM `projectors`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$projectors = $statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);

// all the projectors that are on (logs table doesn't have all the projectors)
$sql = <<<SQL
SELECT `room`, `session`
  FROM `logs`
 WHERE `ended` = TIMESTAMP(0)
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$on_projectors = $statement->fetchAll(PDO::FETCH_KEY_PAIR);

function send($projector, $on) {
    $url = $projector['url'];
    $base = base64_encode($projector['user'] .':'. $projector['pass']);
    $request = curl_init($url . '/cgi-bin/directsend?KEY=3B');
    curl_setopt_array($request, array(
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "referer: http://$url/cgi-bin/webconf",
            "authorization: Basic $base",
        ),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FAILONERROR => true,
    ));
    // send the power button twice if the projector is on
    for ($i = 0; $i < (!$on ? 2 : 1); $i++) {
        $response = curl_exec($request);
        if (curl_errno($request)) {
            info("error: " . curl_error($request));
        }
    }
    curl_close($request);
}

info("_POST is\n");
var_dump($_POST);
// $_POST has a list of projectors that are wanted on

foreach ($projectors as $room => $info) {
    // don't update projectors that have not changed their state
    info("checking $room ... ");
    $is_on = isset($on_projectors[$room]);
    $wanted_on = (isset($_POST[$room]) and $_POST[$room] == 'on');
    info("projector $room is on? ".($is_on ? 'yes' : 'no')." and is wanted on? ".($wanted_on ? 'yes' : 'no'));
    
    if ($wanted_on and !($is_on)) {
        info("turning on\n");
        send($info, 1);
        // there was no log entry for this session so create on
        $sql = <<<SQL
INSERT INTO `logs`
        SET `room` = ?, `started` = NOW(), `by` = ?;
SQL;
        $statement = $db->prepare($sql);
        $statement->execute(array($room, $_SERVER['REMOTE_USER']));
    }
    else if (!($wanted_on) and $is_on) {
        info("turning off\n");
        send($info, 0);
        // update the existing session entry
        $sql = <<<SQL
UPDATE `logs`
   SET `ended` = NOW()
 WHERE `session` = ?;
SQL;
        $statement = $db->prepare($sql);
        $statement->execute(array($on_projectors[$room])); // that's the session
    }
    else {
        info("skipping\n");
    }
}
// send them back to index.php
header('Location: index.php');
?>
