<?php
/* display the web interface */

// engr template
$display_name = "Projectors";
$page_title = "Projector status";
$breadcrumbs = array("status");

// start a connection to the database
// TODO will this pool if there are multiple simultaneous conenctions?
require_once('database.php');
global $db;

$sql = <<<SQL
SELECT `room`, `start`, `end`, `start_by`
FROM `logs`
ORDER BY `room`, `start` DESC
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_GROUP);

$ongoing = '0000-00-00 00:00:00';
$toggles = "";
$logs = "";
foreach ($result as $room => $log) {
    // show how long the project has before a timeout
    if ($log[0]['end'] == $ongoing) {
        $on = 'checked';
        $timeout = 'auto-off at ';
        $d = new DateTime($log[0]['start']);
        $d->modify('+3 hours');
        $timeout .= $d->format('g:i a');
    } else {
        $on = $timeout = '';
    }
    $toggles .= <<<EOT
<label class="toggle">
    <input type="checkbox" name="$room" $on>
    <h5><span class="upper">$room</span>$timeout</h5>
</label>
EOT;
    $room_log = "";
    foreach ($log as $entry) {
        $start = new DateTime($entry['start']);
        $end = $entry['end'] == $ongoing ? 
               'ongoing' : (new DateTime($entry['end']))->format('g:i a');
        $room_log .= <<<EOT
<li>
    <div>{$start->format('g:i a')} - {$end}</div>
    <div>{$start->format('l, M j')}</div>
    <div>By: {$entry['start_by']}</div>
</li>
EOT;
    }
    $logs .= <<<EOT
<div class="logs">
    <h5 class="upper">$room</h5>
    <ul>$room_log</ul>
</div>
EOT;
}
$now = (new DateTime())->format('l, M j \a\t g:i a');
$content = <<<EOT
<div class="colRow colBoxes">
    <div class="col columnbox">
        <h3>Power</h3>
        <form action="update.php" method="post">
            $toggles
            <input type="submit" value="Apply">
        </form>
        <span>Last updated: $now</span>
    </div>
    <div class="col columnbox right">
        <h3>Logs</h3>
        $logs
    </div>
</div>
EOT;
require_once($_SERVER['DOCUMENT_ROOT'] . '/template/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/template/blank-edge-engn.php');
