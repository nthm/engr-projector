<?php
/* updates the state of all projectors, turning them on or off as defined by
 * the $_GET parameter
 * 
 * this script can be called with an empty $_GET to correct the system
 */

// only check authorization if called outside of command line
if (PHP_SAPI !== 'cli') {
    require_once('auth.php');
    $user = $_SERVER['REMOTE_USER'];
    $auth = auth_level($user);
    if (!in_array($auth, array('admin', 'power'), true)) {
        info("not authorized, bye");
        goto done;
    }
} else {
    $user = 'Server';
}
require_once('database.php');

// all the projectors, their active session and who started it
// `by` is needed to prevent turning off projectors that you didn't start
$sql = <<<SQL
   SELECT `projectors`.`room`,
          `url`,
          `user`,
          `pass`,
          `logs`.`open`,
          `logs`.`duration`,
          `logs`.`by`
     FROM `projectors`
LEFT JOIN ( SELECT `room`,
                   `session` AS `open`,
                   TIMESTAMPDIFF(MINUTE, `start`, NOW()) AS `duration`,
                   `start_by` AS `by`
              FROM `logs`
             WHERE `logs`.`end` = TIMESTAMP(0) )
       AS `logs`
       ON `logs`.`room` = `projectors`.`room`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$projectors = $statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);

$sql_update = <<<SQL
UPDATE `logs`
   SET `end` = NOW(), `end_reason` = :reason
 WHERE `session` = :session
SQL;

$sql_insert = <<<SQL
INSERT INTO `logs`
        SET `room` = :room,
            `start_by` = :by
SQL;

define('OFF_WEB'         , 1);
define('OFF_OTHER'       , 2);
define('UNAUTHORIZED'    , 3);
define('SESSION_TRANSFER', 4);
define('TIMED_OUT'       , 5);
define('CONNECTION_LOST' , 6);

foreach ($projectors as $room => $data) {
    info("checking $room ... ");
    if (isset($_GET[$room]) and in_array($_GET[$room], array('0', '1'))) {
        $signal = $_GET[$room];
        info("signal set to $signal");
    } else {
        // just correct the state as needed
        $signal = NULL;
    }
    // set $db_{url,user,pass,open,duration,by}
    // extract($data, EXTR_PREFIX_ALL, "db_"); // doesn't work...
    $url = $data['url'];
    $open = $data['open'];
    $duration = $data['duration'];

    // is projector on or off?
    $snmp_oid = 'enterprises.1248.4.1';
    $ping = fsockopen($url, 80, $errCode, $errStr, 1);
    if ($ping and $snmp = snmpwalk($url, 'projector', $snmp_oid, 10000)) {
        fclose($ping);
        $on = count($snmp) > 1;
    } else {
        info("can't ping $room");
        // regardless of if it's wanted on or off ($signal), there's nothing we can do
        // tell index.php it's broken through the $_SESSION?
        if ($open) {
            $v[':reason'] = CONNECTION_LOST;
            info("TODO find a way to write to the database....");
            //$statement = $db->prepare($sql);
            //$statement->execute($v);
        }
        info("next");
        continue;
    }
    
    info("$room:");
    info("\t power: " . ($on ? 'on' : 'off'));
    info("\t wanted: " . ($signal !== NULL ? ($signal ? 'on' : 'off') : 'update'));
    info("\t session: " . ($open ? 'open' : 'closed'));
    
    $sql = "";
    $v = array();
    if ($on) {
        if ($open) {
            $sql = $sql_update;
            $v[':session'] = $open;
            // check for a timeout
            if ($signal == NULL and $duration > (3 * 60)) {
                $v[':reason'] = TIMED_OUT;
                $signal = 0;
            } else if ($signal == 1) {
                $sql .= "; " . $sql_insert;
                $v[':room'] = $room;
                $v[':by'] = $user;
                $v[':reason'] = SESSION_TRANSFER;
            } else if ($signal == 0) {
                if ($user !== $data['by'] or $auth !== 'admin') {
                    // tell index.php it's an issue through the $_SESSION?
                    info("can't turn off that projector it's not yours");
                    continue;
                }
                $v[':reason'] = OFF_WEB;
            }
        } else {
            $sql = $sql_insert . ", `end` = NOW(), `end_reason` = :reason";
            $v[':room'] = $room;
            $v[':by'] = $user;
            $v[':reason'] = UNAUTHORIZED;
            if ($signal == 1) {
                $sql .= "; " . $sql_insert;
            } else {
                $signal = 0;
            }
        }
    } else { // off
        if ($open) {
            $sql = $sql_update;
            $v[':session'] = $open;
            $v[':reason'] = OFF_OTHER;
        }
        if ($signal == 1) {
            // bug where ; is placed before the query
            $sql .= "; " . $sql_insert;
            $v[':room'] = $room;
            $v[':by'] = $user;
        }
    }
    // send sql?
    if ($sql) {
        info('sending sql');
        info('sql is:');
        info($sql);
        info(print_r($v));
        $statement = $db->prepare($sql);
        $statement->execute($v);
    }
    // send signal to projector?
    info("on is $on and signal is $signal");
    if ($signal === NULL or $on == $signal) {
        continue;
    }
    //
    info("sending $signal to $room (NOT REALLY)");
    continue;
    //
    $base = base64_encode($data['user'] .':'. $data['pass']);
    $request = curl_init($url . '/cgi-bin/directsend?KEY=3B');
    curl_setopt_array($request, array(
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "referer: http://$url/cgi-bin/webconf",
            "authorization: Basic $base",
        ),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FAILONERROR => true,
    ));
    // send the power button twice if the projector is on
    for ($i = 0; $i < ($on ? 2 : 1); $i++) {
        $response = curl_exec($request);
        if (curl_errno($request)) {
            info("error: " . curl_error($request));
        }
    }
    curl_close($request);
}
// https://stackoverflow.com/a/7655379
function info($msg) {
    echo "<pre>" . $msg . "</pre>\n";
    //error_log($msg, 3, 'update.log');
}
done:
info("done");
// send them back to index.php
// header('Location: index.php');
?>
