<?php
// these are copy-paste blocks to save queries - don't run this file
exit();

require_once('database.php');
global $db;

// ----------------------------------------------------------------------------
// all projectors, their power states, and the active session (if running)
$sql = <<<SQL
   SELECT `projectors`.`room`,
          `running`.`session`,
          IF(`running`.`session`, "on", "off") AS "power"
     FROM `projectors`
LEFT JOIN ( SELECT `room`, `session`
              FROM `logs`
             WHERE `logs`.`ended` = TIMESTAMP(0) )
       AS `running`
       ON `running`.`room` = `projectors`.`room`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);
var_dump($result);

// ----------------------------------------------------------------------------
// all the projectors
$sql = <<<SQL
SELECT `room`, `url`, `user`, `pass`
  FROM `projectors`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$projectors = $statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);
var_dump($projectors);

// ----------------------------------------------------------------------------
// all the projectors that are on (logs table doesn't have all the projectors)
$sql = <<<SQL
SELECT `room`, `session`
  FROM `logs`
 WHERE `ended` = TIMESTAMP(0)
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$on_projectors = $statement->fetchAll(PDO::FETCH_KEY_PAIR);
var_dump($on_projectors);

// ----------------------------------------------------------------------------
// old log table which used DATETIME and had no `ended_by`
$logs_table = <<<SQL
CREATE TABLE IF NOT EXISTS `logs` (
    `session` INT         NOT NULL AUTO_INCREMENT,
    `room`    VARCHAR(5)  NOT NULL,
    `started` DATETIME    NOT NULL,
    `ended`   DATETIME    NOT NULL,
    `by`      VARCHAR(50) NOT NULL,
    PRIMARY KEY (`session`),
    FOREIGN KEY (`room`) REFERENCES projectors(`room`)
);
ALTER TABLE `logs`
     ADD UNIQUE `uq_logs`(`room`, `ended`);
SQL;
$db->query($logs_table);

// ----------------------------------------------------------------------------
// projectors, their sessions (or NULL if never on), user, and off reason
$sql = <<<SQL
   SELECT `projectors`.`room`,
          `url`,
          `start`,
          `start_by` AS `user`,
          `end`,
          `end_reasons`.`text` AS `reason`
     FROM `projectors`
LEFT JOIN `logs`
       ON `projectors`.`room` = `logs`.`room`
LEFT JOIN `end_reasons`
       ON `end_reason` = `id`;
SQL;
$statement = $db->prepare($sql);
$statement->execute();

while (($row = $statement->fetch()) !== false) {
    echo "{$row['room']}: {$row['url']} started on {$row['start']} by ";
    echo "{$row['user']} and ended via: {$row['reason']}\n";
}
?>