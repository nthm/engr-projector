<?php
require_once('auth.php');
require_once('database.php');
global $db;

/* render the web interface:
 * steps:
 * ask the database for all projectors and if they're running. build the
 * navigation section using that
 * 
 * TODO: this requires two tables, and visits the `logs` table even if the
 * user will not be able to see logs. efficiency?
 * 
 * use the $_GET variable to know what projector to display on the page. if
 * it's empty, use the first option. build the power button (disabled if the
 * user doesn't have access) and write generic data
 * 
 * if admin, load 5 log entries and calculate statistics with them
 * 
 * note that this script doesn't write to the database in any way. it passes
 * any work to update.php - the "backend" - which does that. this script and
 * the watchdog are both "frontends"
 */

// load all projectors and their active session (if running; else NULL)
$sql = <<<SQL
   SELECT `projectors`.`type`,
          `projectors`.`room`,
          `running`.`session`
     FROM `projectors`
LEFT JOIN ( SELECT `room`, `session`
              FROM `logs`
             WHERE `logs`.`end` = TIMESTAMP(0) )
       AS `running`
       ON `running`.`room` = `projectors`.`room`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$list = $statement->fetchAll(PDO::FETCH_GROUP);
// TODO: fetchAll vs while(fetch() !== false)

$content = "<nav id='list'>";

// stackoverflow.com/q/1921421/first-element-of-array
reset($_GET);
$view = key($_GET);
if ($view === NULL) {
    // load the first projector instead - database must have 1 projector
    $view = reset($list)[0]['room'];
}
// true when $view is validated in the loop below
$ok = false;
// true if the $ok'd projector is running
$on = false;

// navigation
foreach ($list as $type => $rooms) {
    $content .= "<h2>{$type}s</h2><ul>";
    foreach ($rooms as $group) {
        // sets $room and $session
        // stackoverflow.com/q/28232945/destructuring-associative-arrays
        extract($group);
        if ($ok === false) {
            $on = $session !== NULL;
            $ok = $view === $room;
        }
        // $on and $ok values cannot be used here
        $classes = ($session !== NULL ? 'on ' : '') .
                   ($view === $room ? 'selected' : '');
        $content .= "<a href='/?$room'><li class='$classes'>$room</li></a>";
    }
    $content .= "</ul>";
}
$content .= "</nav><main id='projector'>";

// projector page
// functions need globals. using goto. stackoverflow.com/q/7468836/break-if
if ($ok === false) {
    // should only happen if editing the URL
    $content .= "<div><p>Projector does not exist. Check URL</p></div>";
    goto done;
}
// $view has been validated and can now be used
$checked = ($on ? 'checked' : '');
$param = $view . ($on ? '' : '=1');
$content .= "<div><header><a class='toggle' href='/update.php?$param'>"
         .  "<input type='checkbox' $checked><span>Power:</span></a>"
         .  "<h1>ELW $view</h1>";
$sql = <<<SQL
   SELECT `session`,
          DATE_FORMAT(`start`, '%b %e') AS `date`,
          DATE_FORMAT(`start`, '%l:%i %p') AS `start`,
          CONCAT(
              TIMESTAMPDIFF(
                  MINUTE, `start`, IF (`end` = TIMESTAMP(0), NOW(), `end`)
              ), ' min') AS `duration`,
          `start_by` AS `by`,
          `end_reasons`.`text` AS `via`
     FROM `logs`
LEFT JOIN `end_reasons`
       ON `end_reason` = `id`
    WHERE `room` = ?
 ORDER BY `end`
    LIMIT 5
SQL;
$statement = $db->prepare($sql);
$statement->execute(array($view));
$logs = $statement->fetchAll();

if ($on) {
    // there is at least one log if $on is set
    $left = 3 * 60 - $logs[0]['duration']; // hours
    $content .= "<p>Running since {$logs[0]['date']}, {$logs[0]['start']}"
             .  " - $left minutes left</p>";
} else {
    $content .= "<p>Idle</p>";
}
$content .= "</header>"
         .  "<p>Support: Lynn ELW A203</p></div>";
// logs
if (auth_level($_SERVER['REMOTE_USER']) !== 'admin') {
    goto done;
}
$content .= "<h2>Logs</h2>";
if (empty($logs)) {
    $content .= "<div><p>No logs</p></div>";
    goto done;
}
$heads = "";
foreach (['#', 'Date', 'Started', 'Duration', 'By', 'Ended via'] as $th) {
    $heads .= "<th>$th</th>";
}
$content .= "<table id='logs'><thead><tr>$heads</tr></thead><tbody>";
foreach ($logs as $log) {
    $row = "";
    foreach ($log as $data) {
        $row .= "<td>$data</td>";
    }
    $content .= "<tr>$row</tr>";
}
$content .= "</tbody></table>";

// exit
done:
$content .= "</main>";

// configure and load the page template
$display_name = "Projectors";
$page_title = "Projector status";
$breadcrumbs = array("status");

require_once($_SERVER['DOCUMENT_ROOT'] . '/template/config.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/template/blank-edge-engn.php');
?>