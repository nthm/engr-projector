<?php
function auth_groups($netlink) {
    $LDAPUrl    = 'ldaps://[redacted]/';
    $LDAPBindDN = 'cn=Faculty of Engineering Web,ou=ENGN,ou=administrators,dc=uvic,dc=ca';
    $LDAPBindPW = '[redacted]';

    $info = false;
    $ldapconn = ldap_connect($LDAPUrl);
    if ($ldapconn) {
        // bind
        if (ldap_bind($ldapconn, $LDAPBindDN, $LDAPBindPW)) {
            $dn     = "ou=People,dc=uvic,dc=ca";
            $filter = "(&(uid=" . $netlink . ")(objectClass=uvicEduENGR))";
            $search = array("uvicEduENGRmemberOf");

            $sr = ldap_search($ldapconn, $dn, $filter, $search);
            $info = ldap_get_entries($ldapconn, $sr);
            ldap_free_result($sr);
        }
        ldap_close($ldapconn);
    }
    if (is_array($info) && array_key_exists(0, $info))
        $retinfo = $info[0]['uviceduengrmemberof'];
    else
        $retinfo = array();
    return $retinfo;
}

function auth_level($netlink) {
    $groups = auth_groups($netlink);
    foreach (array('admin', 'power') as $level) {
        if (in_array("service:engn:projector:$level", $groups)) {
            return $level;
        }
    }
    return 'viewer';
}
?>
