<?php
/* updates the state of all projectors, turning them on or off as defined by
 * the $_GET parameter
 * 
 * this script can be called with an empty $_GET to correct the system
 */

// send them back to index.php
header('Location: index.php');
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// only check authorization if called outside of command line
if (PHP_SAPI !== 'cli') {
    require_once('auth.php');
    $user = $_SERVER['REMOTE_USER'];
    $auth = auth_level($user);
    if (!in_array($auth, array('admin', 'power'), true)) {
        error_log("$user is not authorized");
        exit();
    }
} else {
    $user = 'Server';
}
require_once('database.php');

// all the projectors, their active session and who started it
// `by` is needed to prevent turning off projectors that you didn't start
$sql = <<<SQL
   SELECT `projectors`.`name`,
          `url`,
          `user`,
          `pass`,
          `logs`.`session`,
          `logs`.`duration`,
          `logs`.`by`
     FROM `projectors`
LEFT JOIN ( SELECT `name`,
                   `session`,
                   TIMESTAMPDIFF(MINUTE, `start`, NOW()) AS `duration`,
                   `start_by` AS `by`
              FROM `logs`
             WHERE `logs`.`end` = TIMESTAMP(0) )
       AS `logs`
       ON `logs`.`name` = `projectors`.`name`
SQL;
$statement = $db->prepare($sql);
$statement->execute();
$projectors = $statement->fetchAll(PDO::FETCH_GROUP|PDO::FETCH_UNIQUE);

function update($reason) {
    global $sql, $sql_values, $session;
    $update = <<<SQL
UPDATE `logs`
   SET `end` = NOW(), `end_reason` = :reason
 WHERE `session` = :session
SQL;
    $sql_values['reason'] = $reason;
    $sql_values['session'] = $session;
    if (!empty($sql))
        $sql .= "; ";
    $sql .= $update;
}
function insert($extra = '') {
    global $sql, $sql_values, $name, $user;
    $insert = <<<SQL
INSERT INTO `logs`
        SET `name` = :name,
            `start_by` = :by
SQL;
    $sql_values['name'] = $name;
    $sql_values['by'] = $user;
    if (!empty($sql))
        $sql .= "; ";
    $sql .= $insert . ($extra);
}
define('OFF_WEB'         , 1);
define('OFF_OTHER'       , 2);
define('UNAUTHORIZED'    , 3);
define('SESSION_TRANSFER', 4);
define('TIMED_OUT'       , 5);
define('CONNECTION_LOST' , 6);

foreach ($projectors as $name => $data) {
    if (isset($_GET[$name]) and in_array($_GET[$name], array('0', '1'))) {
        $signal = $_GET[$name];
        error_log("signal set to $signal");
    } else {
        // just correct the state as needed
        $signal = NULL;
    }
    // is projector on or off?
    $url = $data['url'];
    $ping = fsockopen($url, 80, $errCode, $errStr, 1);
    $snmp_oid = 'enterprises.1248.4.1';
    if ($ping and $snmp = snmpwalk($url, 'projector', $snmp_oid, 20000)) {
        fclose($ping);
        $on = count($snmp) > 1;
    } else {
        error_log("can't ping $name");
        // regardless of if it's wanted on or off ($signal), there's nothing we can do
        // $sql = "UPDATE `projectors` SET `comment` = 'Offline. See support.' WHERE `name` = ?" ??
        if ($session) {
            update(CONNECTION_LOST);
        }
        error_log("next");
        continue;
    }
    $session = $data['session'];
    
    error_log("$name:");
    error_log("\t power: " . ($on ? 'on' : 'off'));
    error_log("\t wanted: " . ($signal !== NULL ? ($signal ? 'on' : 'off') : 'update'));
    error_log("\t session: " . ($session ? 'open' : 'closed'));
    
    $sql = "";
    $sql_values = array();
    if ($on) {
        if ($session) {
            if ($signal === '1') {
                update(SESSION_TRANSFER);
                insert();
            } else if ($signal === '0') {
                if ($auth !== 'admin' or $user !== $data['by'])
                    continue;
                update(OFF_WEB);
            } else if ($signal === NULL and $data['duration'] > (3 * 60)) {
                update(TIMED_OUT);
                $signal = '0';
            }
        } else {
            insert(", `end` = NOW(), `end_reason` = " . UNAUTHORIZED);
            if ($signal === '1')
                insert();
            else
                $signal = '0';
        }
    } else {
        if ($session)
            update(OFF_OTHER);
        if ($signal === '1')
            insert();
    }
    // send sql?
    if (!empty($sql)) {
        error_log('sending sql');
        error_log($sql);
        error_log(print_r($sql_values));
        $statement = $db->prepare($sql);
        $statement->execute($sql_values);
    }
    // send signal to projector? (using == on purpose)
    if ($signal === NULL or $on == $signal) {
        continue;
    }
    error_log("sending $signal to $name");
    $base = base64_encode($data['user'] .':'. $data['pass']);
    $request = curl_init($url . '/cgi-bin/directsend?KEY=3B');
    curl_setopt_array($request, array(
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "referer: http://$url/cgi-bin/webconf",
            "authorization: Basic $base",
        ),
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FAILONERROR => true,
    ));
    // send the power button twice if the projector is on
    for ($i = 0; $i < ($on ? 2 : 1); $i++) {
        $response = curl_exec($request);
        if (curl_errno($request)) {
            error_log("error: " . curl_error($request));
        }
    }
    curl_close($request);
}
?>
